package com.example.avillamayor.nagios_app.Adapter;

import com.example.avillamayor.nagios_app.Callback.NagiosService;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NagiosAdapter {

    public static NagiosService API_SERVICE;

    public static NagiosService getApiService() {

        //Creando el Interceptor, y opciones de log Level
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);
        //Añadiendo logging al ultimo interceptor
        httpClient.addInterceptor(logging);

        String BASE_URL = "http://nagios.winworld.es:8090";

        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())// <-- Uso de log level
                    .build();
            API_SERVICE = retrofit.create(NagiosService.class);
        }
        return API_SERVICE;
    }

}
