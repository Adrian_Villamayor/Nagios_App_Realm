package com.example.avillamayor.nagios_app.Response;

import com.example.avillamayor.nagios_app.Model.Host;
import com.google.gson.annotations.SerializedName;

import java.util.Map;
import java.util.Set;

import io.realm.RealmModel;

public class NagiosContent {

    @SerializedName("content")
    private Map<String, Host> content;

    @SerializedName("success")
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public Map<String, Host> getContent() {
        return content;
    }

}