package com.example.avillamayor.nagios_app.data;

import android.app.Application;
import android.app.Service;

import com.example.avillamayor.nagios_app.Model.Host;
import com.example.avillamayor.nagios_app.Model.Services;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;


public class RealmController {


    //find all objects in the Host.class
    public RealmResults<Host> getHosts(Realm realm) {
        String[] fieldnames = {"services.pING.currentState", "name"};
        Sort[] sortArray = {Sort.DESCENDING, Sort.ASCENDING};
        return realm.where(Host.class).findAllSorted(fieldnames, sortArray);
    }

}
