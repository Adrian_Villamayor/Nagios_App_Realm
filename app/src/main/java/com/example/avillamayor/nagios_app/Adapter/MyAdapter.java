package com.example.avillamayor.nagios_app.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.avillamayor.nagios_app.Model.Host;
import com.example.avillamayor.nagios_app.R;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.HostViewHolder> {

    private final List<Host> hosts;
    private final Context context;

    public MyAdapter(Context context, ArrayList<Host> item) {
        this.context = context;
        hosts = item;

    }

    @Override
    public HostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item, parent, false);

        return new HostViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final HostViewHolder holder, int position) {
        holder.mPhoto.setImageDrawable(getImageDrawable(hosts.get(position)));
        holder.mName.setText(hosts.get(position).getName());

    }

    private Drawable getImageDrawable(Host host) {
        if (host.getServices().getpING() != null) {

            if (host.getServices().getpING().getCurrentState().equals("0")) {
                return context.getResources().getDrawable(R.drawable.host_ok);
            } else if (host.getServices().getpING().getCurrentState().equals("1")) {
                return context.getResources().getDrawable(R.drawable.host_warn);
            } else if (host.getServices().getpING().getCurrentState().equals("2")) {
                return context.getResources().getDrawable(R.drawable.host_critic);
            } else if (host.getServices().getpING().getCurrentState().equals("3")) {
                return context.getResources().getDrawable(R.drawable.hots_unknow);
            } else {
                return context.getResources().getDrawable(R.drawable.cross);
            }
        }
        return context.getResources().getDrawable(R.drawable.cross);

    }

    public static class HostViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        private final TextView mName;
        private final ImageView mPhoto;

        private HostViewHolder(View v) {
            super(v);
            cv = (CardView) v.findViewById(R.id.cv);
            mName = (TextView) v.findViewById(R.id.Name);
            mPhoto = (ImageView) itemView.findViewById(R.id.Status);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public int getItemCount() {
        return hosts.size();
    }

}