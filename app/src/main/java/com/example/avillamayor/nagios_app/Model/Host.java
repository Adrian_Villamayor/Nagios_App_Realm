package com.example.avillamayor.nagios_app.Model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Host extends RealmObject {

    @PrimaryKey
    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SerializedName("services")
    public Services services;

    public void setServices(Services services) {
        this.services = services;
    }

    public Services getServices() {
        return services;
    }

}


