package com.example.avillamayor.nagios_app;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.avillamayor.nagios_app.Adapter.MyAdapter;
import com.example.avillamayor.nagios_app.Adapter.NagiosAdapter;
import com.example.avillamayor.nagios_app.Model.Host;
import com.example.avillamayor.nagios_app.Response.NagiosContent;
import com.example.avillamayor.nagios_app.data.RealmController;

import java.util.ArrayList;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.avillamayor.nagios_app.R.id.recyclerView;

public class MainActivity extends AppCompatActivity {

    private Button mReload;
    private ProgressDialog mDialog;
    private RecyclerView mRecyclerView;
    private MyAdapter myAdapter;
    private Realm realm;
    private RealmController controller;
    private CountDownTimer timer = new CountDownTimer(3000, 3000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            hideProgressDialog();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        controller = new RealmController();

        setContentView(R.layout.activity_main);
        loadNagiosFeed();
        mReload = (Button) findViewById(R.id.reload);
        mReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNagiosFeed();
            }
        });

    }

    private void setRecyclerView(ArrayList<Host> hosts) {
        mRecyclerView = (RecyclerView) findViewById(recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        myAdapter = new MyAdapter(this, hosts);
        mRecyclerView.setAdapter(myAdapter);
    }

    private void getFeed() {
        Call<NagiosContent> call = NagiosAdapter.getApiService().getAllNagios();
        call.enqueue(new Callback<NagiosContent>() {

            @Override
            public void onResponse(Call<NagiosContent> call, Response<NagiosContent> response) {
                if (response.isSuccessful()) {
                    NagiosContent nagiosContent = response.body();
                    if (nagiosContent.isSuccess()) {
                        onGetContentSuccess(nagiosContent);
                    } else {
                        onGetContentServerError(nagiosContent, response.code());
                    }
                } else {
                    onGetContentServerError(response.body(), response.code());
                }
            }

            @Override
            public void onFailure(Call<NagiosContent> call, Throwable t) {
                onGetContentFailure(t);
            }
        });
    }

    private void onGetContentFailure(Throwable t) {
        hideProgressDialog();
        showServerError();
    }

    private void onGetContentServerError(NagiosContent nagiosContent, int code) {
        hideProgressDialog();
        showServerError();
    }

    private void onGetContentSuccess(NagiosContent nagiosContent) {
        hideProgressDialog();

        ArrayList<Host> hosts = new ArrayList<>();

        for (String key : nagiosContent.getContent().keySet()) {
            final Host host = nagiosContent.getContent().get(key);
            host.setName(key);
            hosts.add(host);

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(host);
                }
            });

        }

        loadBBDD();

    }

    private void hideProgressDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private void showServerError() {
        Snackbar.make(mRecyclerView, "Server connection failure.", Snackbar.LENGTH_LONG).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // La Network existe y funciona
            isAvailable = true;
        }
        return isAvailable;
    }

    private void loadNagiosFeed() {

        mDialog = new ProgressDialog(MainActivity.this);
        mDialog.setMessage("Loading Nagios Data...");
        mDialog.setCancelable(true);
        mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mDialog.setIndeterminate(true);
        mDialog.show();

        if (isNetworkAvailable()) {

            getFeed();

        } else {

            Toast.makeText(this, "Without connection, loading data from cache ...", Toast.LENGTH_LONG).show();
            loadBBDD();
        }

    }

    private void loadBBDD() {

        timer.start();

        if (realm != null) {

            ArrayList<Host> hosts = new ArrayList<>();

            for (Host a : controller.getHosts(realm)) {

                hosts.add(a);
            }

            setRecyclerView(hosts);

        } else {
            Toast.makeText(this, "No data in the BBDD.", Toast.LENGTH_LONG).show();
            hideProgressDialog();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
        }
    }
}

